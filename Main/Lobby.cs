using System.Collections.Generic;
using System.Linq;

namespace SteamGameServer.Main
{
    public class Lobby
    {
        public ulong LobbyID;
        public List<Player> Players = new List<Player>();

        public Lobby(ulong lobbyID)
        {
            LobbyID = lobbyID;
        }

        public Player AddPlayerByID(ulong steamID)
        {
            Player player = new Player(steamID);
            Players.Add(player);
            return player;
        }

        public Player GetPlayerByID(ulong steamID)
        {
            Player player = Players.First(x => x.SteamID == steamID);
            return player;
        }

        public void RemovePlayerByID(ulong steamID)
        {
            Player player = Players.First(x => x.SteamID == steamID);
            Players.Remove(player);
        }
        
    }
}