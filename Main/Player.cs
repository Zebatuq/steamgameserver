using System.Text;
using Facepunch.Steamworks;

namespace SteamGameServer.Main
{
    public class Player
    {
        public ulong LobbyID;
        public ulong SteamID;
        public Vector3Net Position = new Vector3Net(0, 0, 0);
        public Inventory Inventory = new Inventory();

        public Player(ulong steamID)
        {
            SteamID = steamID;
            LobbyID = 0;
        }
        
        public Player ChangeLobby(ulong lobbyId)
        {
            Games.GetLobbyByID(LobbyID).RemovePlayerByID(SteamID);
            LobbyID = lobbyId;
            Games.GetLobbyByID(lobbyId).AddPlayerByID(SteamID);
            return this;
        }

        public Player SendP2P(string text, int channel)
        {
            byte[] message = Encoding.UTF8.GetBytes(text);
            Server.Instance.Networking.SendP2PPacket(SteamID, message, message.Length, Networking.SendType.Reliable, channel);
            return this;
        }
    }
}