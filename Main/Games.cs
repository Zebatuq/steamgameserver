using System.Collections.Generic;
using System.Linq;

namespace SteamGameServer.Main
{
    public static class Games
    {
        public static List<Lobby> Lobbies = new List<Lobby>();

        public static Lobby GetLobbyByID(ulong lobbyID)
        {
            return Lobbies.First(x => x.LobbyID == lobbyID);
        }

        public static Lobby CreateLobby(ulong lobbyID)
        {
            Lobby lobby = new Lobby(lobbyID);
            Lobbies.Add(lobby);
            return lobby;
        }

        public static Player CreatePlayer(ulong steamID)
        {
            Player player = new Player(steamID);
            GetLobbyByID(0).Players.Add(player);
            return player;
        }
    }
}