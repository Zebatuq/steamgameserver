using System.IO;
 using Newtonsoft.Json.Linq;
 
 namespace SteamGameServer
 {
     public static class Config
     {
         public static uint AppID;
         public static string ModDir;
         public static string GameDesc;
         public static string ServerName;
         public static int TickRate;
 
         
         // TODO Проверка перез загрузкой
         public static void LoadConfig()
         {
             string rawConfig = File.ReadAllText("config.json");
             JObject config = JObject.Parse(rawConfig);
             
             AppID = (uint) config["AppID"] ;
             ModDir = (string) config["ModDir"];
             GameDesc = (string) config["GameDesc"];
             ServerName = (string) config["ServerName"];
             TickRate = 1000 / (int) config["TickRate"];
             
         }

     }
 }