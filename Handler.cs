using System;
using System.Collections.Generic;
using System.Text;
using Facepunch.Steamworks;
using SteamGameServer.Main;

namespace SteamGameServer
{
    public static class Handler
    {
        public static void JoinHandler(ulong steamID, byte[] data, int length)
        {
            string sessionData = Encoding.UTF8.GetString(data, 0, length);
            if (sessionData == "le")
            {
                Server.Instance.Auth.EndSession(steamID);
            }
            else
            {
                Server.Instance.Auth.EndSession(steamID);
                Server.Instance.Auth.StartSession(data, steamID);
                Games.CreatePlayer(steamID);
            }
        }

        public static void LobbyHandler(ulong steamID, List<string> lobbyData)
        {
            switch (lobbyData[0])
            {
                case "lc":
                    ulong lobbyID = Convert.ToUInt64(lobbyData[1]);
                    Games
                        .CreateLobby(lobbyID)
                        .AddPlayerByID(steamID)
                        .ChangeLobby(lobbyID)
                        .SendP2P("lc|OK", 1);
                    break;
            }
        }
    }
}