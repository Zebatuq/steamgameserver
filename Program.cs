﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Facepunch.Steamworks;
using SteamGameServer.Main;

namespace SteamGameServer
{
    internal static class Program
    {
        private static void Main()
        {
            // Подружаем файл конфигурации
            Config.LoadConfig();

            // Задаем параметры и создаем новый сервер
            ServerInit serverInit = new ServerInit(Config.ModDir, Config.GameDesc);
            Server server = new Server(Config.AppID, serverInit);
            Server.Instance.ServerName = Config.ServerName;

            Server.Instance.Networking.OnIncomingConnection = IncomingConnection;
            Server.Instance.Networking.OnP2PData = P2PData;
            Server.Instance.Auth.OnAuthChange = AuthChange;

            Server.Instance.Networking.SetListenChannel(0, true); // Для получения Тикетов
            Server.Instance.Networking.SetListenChannel(1, true); // Работа с лобби

            Server.Instance.LogOnAnonymous();

            Games.CreateLobby(0);
            while (!Server.Instance.LoggedOn)
            {
                Server.Instance.Update();
                Thread.Sleep(100);
            }

            // Запускаем отдельный таск для постоянного обновления сервера.
            Update().GetAwaiter().GetResult();
        }

        private static async Task Update()
        {
            while (true)
            {
                Thread.Sleep(Config.TickRate);
                Server.Instance.Update();
            }
        }

        private static bool IncomingConnection(ulong steamId)
        {
            Console.WriteLine("Incoming Connection - SteamId: " + steamId);
            return true;
        }

        private static void P2PData(ulong steamid, byte[] data, int length, int channel)
        {
            switch (channel)
            {
                case 0:
                    Task.Run(() => Handler.JoinHandler(steamid, data, length));
                    break;
                case 1:
                    List<string> lobbyData = Encoding.UTF8.GetString(data, 0, length).Split('|').ToList();
                    Task.Run(() => Handler.LobbyHandler(steamid, lobbyData));
                    break;
            }
        }

        private static void AuthChange(ulong steamid, ulong ownerid, ServerAuth.Status status)
        {
            Console.WriteLine(ownerid + " : " + status);
            byte[] message = Encoding.UTF8.GetBytes("t|" + status);
            if (status == ServerAuth.Status.OK)
            {
                Server.Instance.Networking.SendP2PPacket(steamid, message, message.Length, Networking.SendType.Reliable, 0);
            }
            else
            {
                Server.Instance.Networking.SendP2PPacket(steamid, message, message.Length, Networking.SendType.Reliable, 0);
            }
        }
    }
}